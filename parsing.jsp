<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.io.*" %>
<%@ page import="java.text.*,java.util.*,java.sql.*,javax.servlet.*,javax.sql.*,javax.naming.*" %>
<%@ page import = "javax.xml.parsers.*,org.w3c.dom.*" %>

<html>
<head>
<title>KSW01_기상청 XML parsing</title>
<!-- mystyle.css파일 import하여 적용-->
<link rel="stylesheet" type="text/css" href="mystyle.css">
</head>
<body>
<h1>KSW01_기상청 XML parsing</h1>
<%		
		
		// 파싱할 경로 지정
		String url = "http://www.kma.go.kr/wid/queryDFS.jsp?gridx=61&gridy=123";
					// 판교동 http://www.kma.go.kr/wid/queryDFSRSS.jsp?zone=4113565000
		out.println("<b>Parsing URL : </b>"+ url+"<br>");
		//DocumentBuilderFactory 객체 생성(파싱을 위한 준비과정)
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		//DocumentBuilder 객체 생성
		DocumentBuilder builder = factory.newDocumentBuilder();
		//builder를 이용하여 XML을 파싱하여 Document객체 생성
		Document doc = builder.parse(url);

		//생성된 document에서 각 요소들을 접근하여 데이터를 저장함
		Element root = doc.getDocumentElement();
		// doc.getDocumentElement().getNodeName()를 출력하면 XML의 최상위 태그를 가져온다.
		out.println("<b>Root element : </b>" + doc.getDocumentElement().getNodeName()+"<br>");
		// 단일 노드는 Node로 반환
		// 파싱할 태그 접근 : 복수 노드를 선택하기 위한 NodeList로 반환
		NodeList tag_001 = doc.getElementsByTagName("data");
		out.println("<b>파싱할 리스트 수 : </b>" + tag_001.getLength()+"<br>");
		out.println("<b>기상청 동네예보XML 코드설명 : </b> http://www.kma.go.kr/images/weather/lifenindustry/timeseries_XML.pdf <br>"); 
		
		%>
		<br>
		<br>
		<table cellspacing="0" width="100%" border="1">
		<tr>
		<th width="2%">seq</th>	<!--48시간중 몇번째 인지 가르킴(3시간간격)-->
		<th width="7%">hour</th><!--동네예보 3시간단위(15시~18시)-->
		<th width="5%">day</th><!--1번째 날(0:오늘/1:내일/2:모레)-->
		<th width="5%">temp</th><!--현재시간온도-->
		<th width="5%">tmx</th><!--최고온도(-999.0=값이없을경우)-->
		<th width="5%">tmn</th><!--최저온도(-999.0=값이없을경우)-->
		<th width="4%">sky</th><!--하늘상태코드(1:맑음/2:구름조금/3:구름많음/4:흐림)-->
		<th width="4%">pty</th><!--강수상태코드(0:없음/1:비/2:비눈/3:눈비/4:눈)-->
		<th width="5%">wfKor</th><!--날씨한국어-->
		<th width="10%">wfEn</th><!--날씨영어-->
		<th width="5%">pop(%)</th><!--강수확률%-->
		<th width="5%">r12(mm)</th><!--12시간예상강수량-->
		<th width="5%">s12(mm)</th><!--12시간예상적설량-->
		<th width="5%">ws(m/s)</th><!--풍속(m/s)-->
		<th width="4%">wd</th><!--풍향(0:북/1:북동/2:동/3:동남/4:남/5:남서/6:서/7:북서)-->
		<th width="5%">wdKor</th><!--풍향한국어-->
		<th width="5%">wdEn</th><!--풍향영어-->
		<th width="5%">reh(%)</th><!--습도%-->
		<th width="5%">r06(mm)</th><!--6시간예상강수량-->
		<th width="5%">s06(mm)</th><!--6시간예상적설량-->
		</tr>
		
		<%
		// 변수 선언
		String seq = "";
		String hour = "";
		String day = "";
		String temp = "";
		String tmx = "";
		String tmn = "";
		String sky = "";
		String pty = "";
		String wfKor = "";
		String wfEn = "";
		String pop = "";
		String r12 = "";
		String s12 = "";
		String ws = "";
		String wd = "";
		String wdKor = "";
		String wdEn = "";
		String reh = "";
		String r06 = "";
		String s06 = "";
		
		for(int i=0; i<tag_001.getLength(); i++){
			// 각 변수에 XML의 값을 할당
			Element elmt=(Element)tag_001.item(i);
			seq = tag_001.item(i).getAttributes().getNamedItem("seq").getNodeValue();
			hour = elmt.getElementsByTagName("hour").item(0).getFirstChild().getNodeValue();
			day=elmt.getElementsByTagName("day").item(0).getFirstChild().getNodeValue();
			temp=elmt.getElementsByTagName("temp").item(0).getFirstChild().getNodeValue();
			tmx=elmt.getElementsByTagName("tmx").item(0).getFirstChild().getNodeValue();
			tmn=elmt.getElementsByTagName("tmn").item(0).getFirstChild().getNodeValue();
			sky=elmt.getElementsByTagName("sky").item(0).getFirstChild().getNodeValue();
			pty=elmt.getElementsByTagName("pty").item(0).getFirstChild().getNodeValue();
			wfKor=elmt.getElementsByTagName("wfKor").item(0).getFirstChild().getNodeValue();
			wfEn=elmt.getElementsByTagName("wfEn").item(0).getFirstChild().getNodeValue();
			pop=elmt.getElementsByTagName("pop").item(0).getFirstChild().getNodeValue();
			r12=elmt.getElementsByTagName("pop").item(0).getFirstChild().getNodeValue();
			s12=elmt.getElementsByTagName("s12").item(0).getFirstChild().getNodeValue();
			ws=elmt.getElementsByTagName("ws").item(0).getFirstChild().getNodeValue();
			wd=elmt.getElementsByTagName("wd").item(0).getFirstChild().getNodeValue();
			wdKor=elmt.getElementsByTagName("wdKor").item(0).getFirstChild().getNodeValue();
			wdEn=elmt.getElementsByTagName("wdEn").item(0).getFirstChild().getNodeValue();
			reh=elmt.getElementsByTagName("reh").item(0).getFirstChild().getNodeValue();
			r06=elmt.getElementsByTagName("r06").item(0).getFirstChild().getNodeValue();
			s06=elmt.getElementsByTagName("s06").item(0).getFirstChild().getNodeValue();
			
			%>
			<tr>
			<td><%=seq%></td>
			<% // HOUR: Day조건에 따른 출력
			if(day.equals("0")) {
				%><td>금일 <%=hour%>시</td><%
			}else if(day.equals("1")){
				%><td>내일 <%=hour%>시</td><%
			}else if(day.equals("2")){
				%><td>모레 <%=hour%>시</td><%
			}
			%>
			<%	// DAY: 코드에 따른 출력
			if(day.equals("0")) {
				%><td>오늘</td><%
			}else if(day.equals("1")){
				%><td>내일</td><%
			}else if(day.equals("2")){
				%><td>모레</td><%
			}
			%>
			<td><%=temp%><img src="./photo/degree.png" width="25%"></td>
			<td>
			<% // TMX: 자료가 없을 경우 N/A 출력
			if(tmx.equals("-999.0")){
				%><img src="./photo/error.png" width="35%"><%
			}else{
				%><%=tmx%><img src="./photo/degree.png" width="25%"><%
			}
			%>
			</td>
			<td>
			<% // TMN: 자료가 없을 경우 N/A 출력
			if(tmn.equals("-999.0")){
				%><img src="./photo/error.png" width="35%"><%
			}else{
				%><%=tmn%><img src="./photo/degree.png" width="25%"><%
			}
			%>
			</td>
			<td>
			<%	// SKY: 코드에 따른 이미지 출력
			if(sky.equals("1")){
				%><img src="./photo/sun.png" height="200%"><%
			}else if(sky.equals("2")){
				%><img src="./photo/partlycloud.png" height="200%"><%
			}else if(sky.equals("3")){
				%><img src="./photo/mostlycloud.png" height="200%"><%
			}else if(sky.equals("4")){
				%><img src="./photo/cloud.png" height="200%"><%
			}
			%>
			</td>
			<td>
			<%	// PTY: 코드에 따른 이미지 출력
			if(pty.equals("0")){
				%>-<%
			}else if(pty.equals("1")){
				%><img src="./photo/rain.png" height="200%"><%
			}else if(pty.equals("2")){
				%><img src="./photo/rainsnow.png" height="200%"><%
			}else if(pty.equals("3")){
				%><img src="./photo/snowrain.png" height="200%"><%
			}else if(pty.equals("4")){
				%><img src="./photo/snow.png" height="200%"><%
			}
			%>
			</td>
			<td><%=wfKor%></td>
			<td><%=wfEn%></td>
			<td><%=pop%></td>
			<td><%=r12%></td>
			<td><%=s12%></td>
			<!--<td><%=ws%></td>-->
			<td><%=Float.parseFloat(ws)%></td>
			<td>
			<%	// WD: 코드에 따른 이미지 출력
			if(wd.equals("0")){
				%><img src="./photo/n.png" width="35%"><%
			}else if(wd.equals("1")){
				%><img src="./photo/ne.png" width="35%"><%
			}else if(wd.equals("2")){
				%><img src="./photo/s.png" width="35%"><%
			}else if(wd.equals("3")){
				%><img src="./photo/se.png" width="35%"><%
			}else if(wd.equals("4")){
				%><img src="./photo/s.png" width="35%"><%
			}else if(wd.equals("5")){
				%><img src="./photo/sw.png" width="35%"><%
			}else if(wd.equals("6")){
				%><img src="./photo/w.png" width="35%"><%
			}else if(wd.equals("7")){
				%><img src="./photo/nw.png" width="35%"><%
			}
			%>
			</td>
			<td><%=wdKor%></td>
			<td><%=wdEn%></td>
			<td><%=reh%></td>
			<td><%=r06%></td>
			<td><%=s06%></td>
			</tr>
			<%
		}
	
%>
</table>
</body>
</html>